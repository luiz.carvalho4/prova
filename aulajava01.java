package aulasdejava;

public class aulajava01 {

	private int num = 0;
	
	private static aulajava01 aa = new aulajava01();
	private aulajava01() {}
	public static aulajava01 getIns() {
		return aa;
	}
	public int outroNum() {
		int x = 0;
		return ++x;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num=num;
	}
	
}
